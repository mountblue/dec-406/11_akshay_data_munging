import libraries


def min_temp_diff():
    weather = libraries.Munge('Data files/weather.dat')
    result = weather.minimum_difference('MxT', 'MnT')
    print("Day : {}\nDiff(max temp - min temp) : {}".format(result[0][0], result[0][1]))

min_temp_diff()
