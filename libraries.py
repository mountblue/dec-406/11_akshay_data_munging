import re


class Munge:

    def __init__(self, file_path):
        self.file_path = file_path

    def read(self):
        with open(self.file_path) as file:
            file_reader = file.readlines()
            return file_reader

    def file_parser(self):
        file_data = []
        file_reader = self.read()
        for line in file_reader:
            if len(line.split()) > 1:
                line = re.sub(r"\*|-|\d+\.", '', line)
                file_data.append(line.split())

        return file_data

    def minimum_difference(self, col1, col2):
        file_data = self.file_parser()
        result = {}
        max_data = file_data[0].index(col1)
        min_data = file_data[0].index(col2)
        for data in file_data[1:]:
            curr_data = data[0]
            result[curr_data] = abs(int(data[max_data]) - int(data[min_data]))
        result = sorted(result.items(), key=lambda x: x[1])[:1]
        return result
