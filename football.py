import libraries


def min_for_against_diff():
    football = libraries.Munge('Data files/football.dat')
    result = football.minimum_difference('F', 'A')
    print("Team : {}\nGoal Diff(For-Against) : {}".format(result[0][0], result[0][1]))

min_for_against_diff()
