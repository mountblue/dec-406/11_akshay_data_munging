# Data Munging

# Part One: Weather Data

write a program to output the day number (column one) with the smallest temperature spread (the maximum temperature is the second column, the minimum the third column).

# Part Two: Soccer League Table

Write a program to print the name of the team with the smallest difference in ‘for’ and ‘against’ goals.

# Part Three: DRY Fusion

Take the two programs written previously and factor out as much common code as possible, leaving you with two smaller programs and some kind of shared functionality.

